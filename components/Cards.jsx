import * as React from 'react';
import { Avatar, Card, Title } from 'react-native-paper';
import SignUp from './SignUp';
import tendero from '../assets/tendero.jpg'

// const LeftContent = props =>  
//     <Avatar.Image size={40} source={require('../assets/favicon.png')} />
const Cards = () => (
    <Card>
        {/* <Card.Title title="Registra tu Tienda" left={LeftContent} /> */}
        <Card.Cover style={{ width: '100%', height: '50%' }} source={tendero} />
            <Title style={{ display:'flex', justifyContent: 'center', color: 'purple', size: 'bold' }}>Formulario de Registro</Title>
        <Card.Content style={{ flex:1, width: '100%', height: '100%'}}>    
        </Card.Content>
    </Card>
);

export default Cards;