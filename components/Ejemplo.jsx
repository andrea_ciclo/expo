import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet, 
    Text,
    View,
    TextInput,
    TouchableHighLight,
    AlertIOS
} from 'react-native';

export default class Ejemplo extends Component {
    constructor(){
        super()

        this.state={ 
            title: '',
            subtitle: '',
            comment: ''
        }
    }
    changeTitle(title){
        this.setState({title})
    }
    changeSubtitle(subtitle){
        this.setState({subtitle})
    }
    changeComment(comment){
        this.setState({comment})
    }
    buttonPressed(){
        if(this.state.comment && this.state.title && this.state.subtitle){
            AlertIOS.alert(this.state.title+ ''+ this.state.subtitle + " " + this.state.comment)
        } else {
            AlertIOS.alert('Error')
        }
    }
    render() {
        return(
            <View style={styles.container}>
                <Text styles={styles.title}>
                    Welcome to React Native    
                </Text>
                <TextInput
                    style={styles.input}
                    placeholder="Title"
                    value={this.state.title}
                    onChangeText={(title) => this.changeTitle(title)}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Subtitle"
                    value={this.state.subtitle}
                    onChangeText={(subtitle) => this.changeSubtitle(subtitle)}
                />
                <TextInput
                    multiline={true}
                    style={[styles.input, styles.textArea]}
                    placeholder="Comment"
                    value={this.state.comment}
                    onChangeText={(comment) => this.changeComment(comment)}
                />
                <TouchableHighLight 
                style={styles.button}
                onPress={() =>this.buttonPressed()}
                >
                    <Text style={styles.textButton}>Send</Text>
                </TouchableHighLight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red',
        marginTop: 30,
        paddingLeft: 15,
        paddingRight: 15,
    },
    button: {
        backgroundColor: 'skyblue',
        paddingTop: 15,
        paddingBottom: 15
    },
    textButton: {
        textAlign: 'center',
        color: 'white'
    },  
    title: {
        textAlign: 'center',
        fontSize: 18,
        marginBottom: 5
    },
    input: {
        height: 40,
        borderColor: 'black',
        borderWidth: 2,
        marginBottom: 20
    },
    textArea: {
        height: 60,


    },
    
});