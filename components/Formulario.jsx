import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, Text } from 'react-native';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import Pickers from './Pickers';
import Tipo from './Tipo';

import { Card } from 'react-native-elements'

const SignupSchema = Yup.object().shape({
    nombre: Yup.string()
        .max(30, 'Sólo escriba 30 carácteres')
        .required('Required'),
    direccion: Yup.string()
        .max(30, 'Sólo escriba 30 carácteres')
        .required('Required'),
    telefono: Yup.number()
        .max(10, 'Sólo escriba 10 carácteres')
        .required('Required'),
    tendero: Yup.number()
        .max(2, 'Sólo escriba 2 carácteres')
        .required('Required'),
    });


export default class Formulario extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
        nombre: '',
        direccion: '',
        telefono: '',
        tendero: '',
        };
    }
    
    onLogin() {
        const { nombre, direccion, telefono, tendero } = this.state;
        Alert.alert('Credentials', `${nombre} + ${direccion} + ${telefono} + ${tendero}`);
    }

    render() {
        return (
            
            <View style={styles.container}>
                <Card style={styles.card}>
                    <Formik
                        initialValues={{
                            nombre: '',
                            direccion: '',
                            telefono: '',
                            tendero: '',
                        }}
                        validationSchema={SignupSchema}
                        onSubmit={values => {
                            // same shape as initial values
                            console.log(values);
                        }}
                        >
                        {({ errors, touched }) => (
                        <Form>
                    <Text style={{ fontWeight: 'bold', color: '#5CA044' }}>
                    Nombre
                    </Text>
                    <TextInput
                    value={this.state.nombre}
                    onChangeText={(nombre) => this.setState({ nombre })}
                    placeholder={'Coloca aquí tu nombre'}
                    style={styles.input}
                    />
                    {errors.nombre && touched.nombre ? (
                        <div>{errors.nombre}</div>
                    ) : null}
                    <Text style={{ fontWeight: 'bold', color: '#5CA044' }}>
                    Dirección
                    </Text>
                    <TextInput
                    value={this.state.direccion}
                    onChangeText={(direccion) => this.setState({ direccion })}
                    placeholder={'Coloca aquí tu dirección'}
                    style={styles.input}  
                    />
                        {errors.direccion && touched.direccion ? (
                        <div>{errors.direccion}</div>
                    ) : null}
                    <Text style={{ fontWeight: 'bold', color: '#5CA044' }}>
                    Teléfono
                    </Text>
                    <TextInput
                    value={this.state.number}
                    onChangeText={(number) => this.setState({ number })}
                    placeholder={'Coloca aquí tu teléfono'}
                    secureTextEntry={true}
                    style={styles.input}
                    />
                        {errors.telefono && touched.telefono ? (
                        <div>{errors.telefono}</div>
                    ) : null}
                    <Text style={{ fontWeight: 'bold', color: '#5CA044' }}>
                    Tendero
                    </Text>
                    <TextInput
                    value={this.state.tendero}
                    onChangeText={(tendero) => this.setState({ tendero })}
                    placeholder={'Número de tendero'}
                    style={styles.input}
                    />
                        {errors.tendero && touched.tendero ? (
                        <div>{errors.tendero}</div>
                    ) : null}
                    <Text style={{ fontWeight: 'bold', color: '#5CA044' }}>
                    Hora
                    </Text>
                    <Pickers/>
                    <Text style={{ fontWeight: 'bold', color: '#5CA044' }}>
                    Tipo
                    </Text>
                    <Tipo/>
                    <View style={[{ width: "90%", margin: 10, backgroundColor: "red" }]}>
                        <Button onPress={this.buttonClickListener} title="Button Three" color="#1DE9B6"/>
                    </View>
                    </Form>
                        )}
                    </Formik>
                </Card>
            </View> 
        );
    }
    }

    const styles = StyleSheet.create({
    container: {
        display: 'flex',
        /* flex: 0.3, */
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        flexDirection: "column",
        borderColor: "green", 
        marginTop: 20,
        /*  borderRadius: 8,
        borderWidth: 4,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20, */
        marginBottom: 20,
        
    },
    input: {
        width: 400,
        height: 50,
        padding: 15,
        borderWidth: 1,
        borderColor: 'green',
        marginBottom: 20,
        
    },
    card: {
        width: 100,
        height: 100,
    }
    });