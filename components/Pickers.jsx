import React, { useState } from "react";
import { View, Picker, StyleSheet } from "react-native";

const Pickers = () => {
    const [selectedValue, setSelectedValue] = useState("java");

    return (
        <View style={styles.container}>
        <Picker
            selectedValue={selectedValue}
            style={{ height: 30, width: 300, color: "green", fontSize: 15 }}
            onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
            >
            <Picker.Item label="mañana" value="mañana" />
            <Picker.Item label="tarde" value="tarde" />
            <Picker.Item label="noche" value="noche" />
        </Picker>
        </View>
    );
    }

    const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 1,
        paddingBottom: 2,
        alignItems: "center",
        borderColor: "green"
        
    }
    });

export default Pickers;