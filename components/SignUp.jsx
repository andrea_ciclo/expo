import React from 'react'
import { Avatar, Card, Title } from 'react-native-paper';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Platform,
} from 'react-native'
import { Formik, Field } from 'formik'
import * as yup from 'yup'
import CustomInput from './CustomInput'
import { Checkbox } from 'react-native-paper';
import RNPickerSelect, {Picker} from 'react-native-picker-select';
import tendero from '../assets/tendero.jpg'

const signUpValidationSchema = yup.object().shape({
    fullName: yup
      .string()
      .matches(/(\w.+\s).+/, 'Ingresa nombre y apellido')
      .required('Este campo es requerido'),
    address: yup
      .string()
      .matches(/(\w.+\s).+/, 'Ingresa una dirección válida')
      .required('Este campo es requerido'),
    phoneNumber: yup
      .string()
      .matches(/(01)(\d){7}\b/, 'Ingresa un número válido')
      .required('Este campo es requerido'),
    tendero: yup
      .string()
      .matches(/\d/, "Ingresa el número de tendero")
      .min(2, ({ min }) => `Recuerda que son ${min} carácteres`)
      .required('Este campo es requerido'),
    hora: yup
      .string()
      .required('Este campo es requerido'),
    tipo: yup
      .string()
      .required('Este campo es requerido'),
  })
  
const SignUp = () => {
  const [checked, setChecked] = React.useState(false);

  return (
    <>
    <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <View style={styles.signupContainer}>
          <Card.Cover style={{ width: '100%', height: '25%', marginBottom: 20, }} source={tendero}></Card.Cover>
          <Text>Formulario de Registro</Text>
            <Formik
            validationSchema={signUpValidationSchema}
              initialValues={{
                fullName: '',
                address: '',
                phoneNumber: '',
                tendero: '',
                hora: '',
                tipo: '',
              }}
              onSubmit={values => console.log(values)}
            >
              {({ handleChange,
                    handleBlur,
                    handleSubmit,
                    setFieldValue,
                    values,
                    errors,
                    isValid, }) => (
                <>
                  <Field
                    component={CustomInput}
                    name="fullName"
                    placeholder="Nombre"
                  />
                  <Field
                    component={CustomInput}
                    name="address"
                    placeholder="Dirección"
                  />
                  <Field
                    component={CustomInput}
                    name="phoneNumber"
                    placeholder="Teléfono"
                    keyboardType="numeric"
                  />
                  <Field
                    component={CustomInput}
                    name="tendero"
                    placeholder="Tendero"
                    keyboardType="numeric"
                  />
                  <RNPickerSelect 
                    placeholder= {{
                    label: 'Hora',
                    value: '',
                  }}
                    items={[
                      {
                        label: 'Mañana',
                        value: 'Mañana',
                      },
                      {
                        label: 'Tarde',
                        value: 'tarde',
                      },
                      {
                        label: 'Noche',
                        value: 'noche',
                      },
                    ]}
                    onValueChange={value => {
                      console.log(value)
                      }}
                    style={pickerSelectStyles}
                    useNativeAndroidPickerStyle={false}
                  />
                  <RNPickerSelect 
                    placeholder= {{
                      label: 'Tipo',
                      value: '',
                    }}
                    items={[
                      {
                        label: 'A',
                        value: 'a',
                      },
                      {
                        label: 'B',
                        value: 'b',
                      },
                    ]}
                    onValueChange={value => {
                      console.log(value)
                      }}
                    style={pickerSelectStyles}
                    useNativeAndroidPickerStyle={false}
                  />
                  <Button
                    onPress={handleSubmit}
                    title="REGISTRADO"
                    color= 'purple'
                    disabled={!isValid}
                  />
                </>
              )}
            </Formik>
        </View>
      </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#B63E55',
    ...Platform.select({
      android:{
        width: '100%',
        height: '100%',
        padding:20,
      }
    })
  },
  signupContainer: {
    width: '50%',
    height: '90%',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    elevation: 20,
    backgroundColor: '#e6e6e6',
    borderRadius: 10,
    ...Platform.select({
      android:{
        width: '100%',
        height: '80%',
      }
    })
  },
}) 


const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    marginBottom: 10,
    fontSize: 50,
    paddingVertical: 20,
    paddingHorizontal: 50,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 10,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    marginBottom: 10,
    fontSize: 10,
    paddingVertical: 8,
    paddingHorizontal: 290,
    borderWidth: 1,
    padding: 2,
    backgroundColor: 'white',
    borderColor: 'grey',
    borderRadius: 10,
    color: 'white',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputWeb: {
    marginBottom: 10,
    height: 40,
    fontSize: 12,
    paddingVertical: 8,
    paddingHorizontal: 250,
    borderWidth: 1,
    padding: 2,
    elevation: 20,
    backgroundColor: 'white',
    borderColor: 'grey',
    borderRadius: 10,
    color: 'black',
    paddingRight: 5, // to ensure the text is never behind the icon
  },
});

export default SignUp