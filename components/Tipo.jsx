import React, { useState } from "react";
import { View, Picker, StyleSheet } from "react-native";

const Tipo = () => {
    const [selectedValue, setSelectedValue] = useState("java");

    return (
        <View style={styles.container}>
        <Picker
            selectedValue={selectedValue}
            style={{ height: 30, width: 300, color: "green", fontSize: 15 }}
            onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
            >
            <Picker.Item label="A" value="A" />
            <Picker.Item label="B" value="B" />
        </Picker>
        </View>
    );
    }

    const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 5,
        alignItems: "center",
        borderColor: "green"
        
    }
    });

export default Tipo;